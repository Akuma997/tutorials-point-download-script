base_url=$1
echo "[Downloading] "
echo "[Please Note to not use script extensively, it uses wget to crawl the site and download stuffs.]"
echo $base_url
wget -c -r -np -k --header="Accept: text/html" --user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0" $base_url

cd ./www.tutorialspoint.com
pwd
echo "-----"
folder_name=$(echo $base_url|cut -d "/" -f 4)
prepend_name="Free_tutorial"
append_name=".pdf"
echo $folder_name
cd $folder_name
echo "[Downloaded files: ]"
echo "                  "
ls
echo "[Copying the Unification file: ]"
cp ../../Format.html ./Unified.html
ls
files_index=$(cat index.htm | htmlq ".toc" | htmlq -a href a | sed 's/^https:\/\/.*//g')
(cat index.htm | htmlq ".toc" ) >> Unified.html
for each in "${files_index[@]}";
do
    echo "-----"
    echo $each
    (cat $each | htmlq ".tutorial-content") >> Unified.html
    echo "-----"
done
wkhtmltopdf Unified.html $prepend_name$folder_name$append_name




